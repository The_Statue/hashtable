/*******************************************************************************************
*
*   raylib [core] example - Basic window
*
*   Welcome to raylib!
*
*   To test examples, just press F6 and execute raylib_compile_execute script
*   Note that compiled executable is placed in the same folder as .c file
*
*   You can find all basic examples on C:\raylib\raylib\examples folder or
*   raylib official webpage: www.raylib.com
*
*   Enjoy using raylib. :)
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (@raysan5)
*
********************************************************************************************/

#include "raylib.h"
#include "raygui.h"
#include <iostream>

#define RAYGUI_IMPLEMENTATION
#define RAYGUI_SUPPORT_ICONS
#include "raygui.h"
#include "hash.h"

hash* HashFunction = new hash;
std::string hashArray[50];
Rectangle collideBoxRec;
std::string hashWord = "";
int key = 0;
int collide = -1;

int main(int argc, char* argv[])
{
	// Initialization
	//--------------------------------------------------------------------------------------
	int screenWidth = 1600;
	int screenHeight = 900;
	char randomChar;
	int hashedNumber = 0;

	collideBoxRec = { screenWidth * 0.29f, screenHeight * -0.1f, screenWidth * 0.02f,screenHeight * 0.02f };

	InitWindow(screenWidth, screenHeight, "Hash table");

	SetTargetFPS(60);
	//--------------------------------------------------------------------------------------

	// Main game loop
	while (!WindowShouldClose())    // Detect window close button or ESC key
	{
		// Update
		//----------------------------------------------------------------------------------
		key = GetKeyPressed();
		if (IsKeyPressed(KEY_SPACE) || IsKeyPressed(KEY_ENTER))
		{
			collide = -1;
			//Randomise input for testing
			/*hashWord = "";
			int length = (rand() % 9) + 1;
			for (int i = 0; i < length; i++)
			{
				randomChar = 'a' + rand() % 26;
				hashWord += randomChar;
			}*/
			hashedNumber = HashFunction->hashFunction(hashWord.c_str(), hashWord.length());

			if (hashArray[(hashedNumber % 100) / 2] != "") //Detect if there is not a blank value, output a message
			{
				collide = (hashedNumber % 100) / 2;
				std::cout << "Collision! Overwriting '" << hashArray[(hashedNumber % 100) / 2] << "' at " << (hashedNumber % 100) / 2 << " with word '" << hashWord << "'\n";
			}
			hashArray[(hashedNumber % 100) / 2] = hashWord; //set the word into the array

			collideBoxRec.y = collide++ * collideBoxRec.height;
		}

		if (((key > 32) && (key <= 125)) ||
			((key >= 128) && (key < 255)))
		{
			hashWord += (char)key;
		}
		if (IsKeyPressed(KEY_BACKSPACE) && hashWord.length() > 0)
		{
			hashWord.erase(hashWord.size() - 1);
		}
		if (IsKeyPressed(KEY_DELETE) )
		{
			hashWord.erase(0);
		}

		//GuiTextBox(textBoxRec, hashWord, 50, true);

		// Draw
		//----------------------------------------------------------------------------------
		BeginDrawing();
		ClearBackground(RAYWHITE);

		for (int i = 0; i < 50; i++) //Draw the list
		{
			DrawRectangle(0, i * 18, 500, 20, GRAY);
			DrawRectangleLines(0, i * 18, 500, 20, DARKGRAY);
			DrawText(FormatText("%i:", i), 10, i * 18, 18, BLACK);
			DrawText(hashArray[i].c_str(), 50, i * 18, 18, BLACK);
		}

		if (collide != -1)
			DrawRectangleRec(collideBoxRec, RED); // draw if a collision has happened
			//Draw latest input
		DrawText(hashWord.c_str(), screenWidth / 2, screenHeight / 2 - 50 * 1.5, 50, BLACK);
		DrawText(FormatText("Hash: %i", hashedNumber), screenWidth / 2, screenHeight / 2 - 50 * 0.5, 50, BLACK);

		EndDrawing();
		//----------------------------------------------------------------------------------
	}

	// De-Initialization
	//--------------------------------------------------------------------------------------   
	CloseWindow();        // Close window and OpenGL context
	//--------------------------------------------------------------------------------------

	return 0;
}