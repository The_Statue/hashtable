#include "hash.h"
#include "raylib.h"
#include <iostream>

int hash::hashFunction(const char* input, int length)
{
	int hashNum = 0;
	std::cout << std::endl << "Start " << input << std::endl;
	for (uint16_t i = 0; i < length; i++)
	{
		hashNum += input[i] * (i + 1) * 26;
		std::cout << input[i] << ":" << input[i] * (i + 1) << ":" << hashNum << std::endl;
	}
	input = "";
	std::cout << "End" << std::endl;
	return hashNum;
}